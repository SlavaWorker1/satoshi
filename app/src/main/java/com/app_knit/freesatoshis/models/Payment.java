package com.app_knit.freesatoshis.models;

import com.google.gson.annotations.SerializedName;

public class Payment {

    @SerializedName("id")
    private String mId;

    @SerializedName("receiverWallet")
    private String mReceiverWallet;

    @SerializedName("satoshisRequested")
    private long mSatoshisRequested;

    @SerializedName("status")
    private String mStatus;

    @SerializedName("paymentDate")
    private String mPaymentDate;

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getReceiverWallet() {
        return mReceiverWallet;
    }

    public void setReceiverWallet(String mReceiverWallet) {
        this.mReceiverWallet = mReceiverWallet;
    }

    public long getSatoshisRequested() {
        return mSatoshisRequested;
    }

    public void setSatoshisRequested(long mSatoshisRequested) {
        this.mSatoshisRequested = mSatoshisRequested;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public String getPaymentDate() {
        return mPaymentDate;
    }

    public void setPaymentDate(String mPaymentDate) {
        this.mPaymentDate = mPaymentDate;
    }
}
