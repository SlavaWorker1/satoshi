package com.app_knit.freesatoshis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class FirebaseMessageAcitivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_firebase_message_acitivity);
        this.setFinishOnTouchOutside(false);

        TextView tvTitle = (TextView) findViewById(R.id.tvTitle);
        TextView tvMessage = (TextView) findViewById(R.id.tvMessage);
        Button btnConfirm = (Button) findViewById(R.id.btnConfirm);
        tvTitle.setText(getString(R.string.please_update));
        String text = "<a href=" + getString(R.string.google_play_link) +">"+ getString(R.string.update_from_google_play)+ "</a>";
        tvMessage.setText(Html.fromHtml(text));
        tvMessage.setMovementMethod(LinkMovementMethod.getInstance());


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.google_play_link)));
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
