package com.app_knit.freesatoshis.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Abstract base for managers that need to cache data to the internal storage {@link SharedPreferences}
 */
public abstract class SPUser {

    protected static final String UNDERSCORE = "_";
    protected static final String MAX_SUFFIX = UNDERSCORE + "max";

    protected Context mContext;

    protected SPUser(Context context) {
        mContext = context;
    }

    protected SharedPreferences getSharedPreferences(final String tag) {
        return mContext.getSharedPreferences(tag, Context.MODE_PRIVATE);
    }
}
