package com.app_knit.freesatoshis.managers;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.app_knit.freesatoshis.R;
import com.appodeal.ads.Appodeal;
import com.appodeal.ads.InterstitialCallbacks;
import com.appodeal.ads.RewardedVideoCallbacks;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;
import java.util.WeakHashMap;


public class AdRequestManager extends SPUser implements RewardedVideoAdListener {

    public static final int STATE_ELIGABLE_TO_PLAY = 1;

    public static final String AD_COLONY_INTERSTITIAL_ID = "vzaf74ff67e5e54626b2";

    public static long SATOSHIS_PER_TRY = 0;
    public static final int STATE_WAITING = 2;

    public static final String DIRECTORY_NAME = "AdRequestManager";

    public static final long WAIT_TIME_MILIS = 30 * 60 * 1000;

    public static final String START_TIME_KEY = "start_time";
    public static final String REMAINING_TRIES_KEY = "remainingTries";
    public static final int MAX_TRIES = 6;

    private static long sStartTime = -1;
    private static int sRemainingTries = -1;

    private static WeakHashMap<AdRequestStateChangeListener, Void> sListeners = new WeakHashMap<>();
    private static boolean sAdColonyInited = false;

    private TextView mTextToUpdate;

    private static AdRequestManager sInstance;

    private AdColonyManager adColonyManager;
    private AdMobManager adMobManager;


    public static AdRequestManager getInstance(Context context, AdRequestStateChangeListener listener) {
//        if (sInstance == null) {
        sInstance = new AdRequestManager(context, listener);
//        }
        return sInstance;
    }


    private static void markPotentialCheater(Context context) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            FirebaseDatabase.getInstance().getReference()
                    .child("potential_cheaters").child(user.getUid()).child("last_time_change").setValue(simpleDateFormat.format(new Date()));
        }
    }

    static void setStartTime(final long startTime, final Context context) {

        sLock = true;

        fetchServerStart(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final long serverStart = dataSnapshot.getValue() == null ? -1 : (long) dataSnapshot.getValue();
//                boolean shouldMark = false;
//
//                if (serverStart > startTime) {
//                    shouldMark = serverStart - startTime >= WAIT_TIME_MILIS;
//                } else {
//                    shouldMark = startTime - serverStart >= WAIT_TIME_MILIS;
//                }
//
//                if (shouldMark) {
//                    markPotentialCheater(context);
//                }

                getServerTime(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot serverSnapshot) {
                        long delta = ((Long) serverSnapshot.getValue()) - serverStart;
                        sStartTime = System.currentTimeMillis() - delta;
                        sLock = false;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        sLock = false;
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                sLock = false;
            }
        });
    }

    private static boolean sLock = false;
    private static long sLastDiff = -1;

    private static String LAST_DIFF_TICK = "last_diff_tick";

    Handler timerHandler = new Handler();
    Runnable timerRunnable = new Runnable() {

        @Override
        public void run() {

            if (sLock) {
                timerHandler.postDelayed(this, 500);
                return;
            }

            long millis = System.currentTimeMillis() - getStartTime();
            long thirtyMinutes = AdRequestManager.WAIT_TIME_MILIS - millis;
            int seconds = (int) (thirtyMinutes / 1000);
            int minutes = seconds / 60;
            seconds = seconds % 60;

            if (mTextToUpdate != null) {
                mTextToUpdate.setText(String.format("%d:%02d", minutes, seconds));
            }

            if (thirtyMinutes <= 0) {
                sLastDiff = -1;
                setRemainingTries(MAX_TRIES);
            } else {
                timerHandler.postDelayed(this, 500);

                if (getRemainingTries() > 0) {
                    setRemainingTries(0);
                }
            }

            sLastDiff = millis;
        }
    };


    private AdRequestManager(Context context, AdRequestStateChangeListener sAdListener) {
        super(context);

        sListeners.put(sAdListener, null);

        if (sRemainingTries == -1) {

            loadData();
        }

        if (!sAdColonyInited) {
            initInterstitial();
            sAdColonyInited = true;
        }
    }

    private void loadData() {
        // Load remaining lives
        // Load start server time
        // Load current server time

        getServerTime(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final long currentServerTime = (long) dataSnapshot.getValue();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                            .child("users").child(user.getUid());

                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            userDataLoaded(dataSnapshot, currentServerTime);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void userDataLoaded(DataSnapshot dataSnapshot, long currentServerTime) {
        long serverStartTime = dataSnapshot.child("serverStart").getValue() == null ? -1 : (long) dataSnapshot.child("serverStart").getValue();
        Object value = dataSnapshot.child(REMAINING_TRIES_KEY).getValue();
        int remainingTries = MAX_TRIES;

        if (value != null) {
            if (value instanceof Long) {
                remainingTries = ((Long) value).intValue();
            } else if (value instanceof String) {
                remainingTries = Integer.parseInt((String) value);
            } else {
                remainingTries = (int) dataSnapshot.getValue();
            }
        }


        if (remainingTries == 0) {
            long deltaTime = Math.abs(currentServerTime - serverStartTime);
            if (deltaTime >= WAIT_TIME_MILIS) {
                setRemainingTries(MAX_TRIES);
            } else {

//                System.out.println("Time: " + deltaTime);
                sStartTime = System.currentTimeMillis() - deltaTime;

//                System.out.println("Time: " + (System.currentTimeMillis() - sStartTime));

                sRemainingTries = 0;
                startWaitTimer();

                invokeListeners(STATE_WAITING);
            }
        } else {
            sRemainingTries = remainingTries;
        }

        refresh();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child("remainingTries");

            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    int remainingTries = 0;
                    Object value = dataSnapshot.getValue();
                    if (value instanceof Long) {
                        remainingTries = ((Long) value).intValue();
                    } else if (value instanceof String) {
                        remainingTries = Integer.parseInt((String) value);
                    } else if (value != null) {
                        remainingTries = (int) dataSnapshot.getValue();
                    } else {
                        remainingTries = MAX_TRIES;
                    }

                    setRemainingTries(remainingTries);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private void initInterstitial() {
        Appodeal.setInterstitialCallbacks(new InterstitialCallbacks() {
            private Toast mToast;

            @Override
            public void onInterstitialLoaded(boolean isPrecache) {
                showToast("onInterstitialLoaded");
            }

            @Override
            public void onInterstitialFailedToLoad() {
                showToast("onInterstitialFailedToLoad");
            }

            @Override
            public void onInterstitialShown() {
                showToast("onInterstitialShown");

                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        saveRemainingTries(sRemainingTries - 1, null);
//                        setRemainingTries(sRemainingTries - 1);
                    }
                });
            }

            @Override
            public void onInterstitialClicked() {
                showToast("onInterstitialClicked");
            }

            @Override
            public void onInterstitialClosed() {
                showToast("onInterstitialClosed");
                invokeRewardListeners();
            }

            void showToast(final String text) {
                System.out.println("Appoloving,Interstitial " + text);
//                if (mToast == null) {
//                    mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
//                }
//                mToast.setText(text);
//                mToast.setDuration(Toast.LENGTH_SHORT);
//                mToast.show();
            }
        });

        AdColony.setRewardListener(new AdColonyRewardListener() {
            @Override
            public void onReward(AdColonyReward adColonyReward) {
                Log.d("AdColony", "REWARD REWARD REWARD REWARD");
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        saveRemainingTries(sRemainingTries -1, null);
                        setRemainingTries(sRemainingTries - 1);
                        invokeRewardListeners();
                    }
                });
            }
        });

        Appodeal.setRewardedVideoCallbacks(new RewardedVideoCallbacks() {
            private Toast mToast;

            @Override
            public void onRewardedVideoLoaded() {
                showToast("onRewardedVideoLoaded");
            }

            @Override
            public void onRewardedVideoFailedToLoad() {
                showToast("onRewardedVideoFailedToLoad");

            }

            @Override
            public void onRewardedVideoShown() {
                ((Activity) mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setRemainingTries(sRemainingTries - 1);
                    }
                });
                showToast("onRewardedVideoShown");
            }

            @Override
            public void onRewardedVideoFinished(int amount, String name) {
                showToast(String.format("onRewardedVideoFinished. Reward: %d %s", amount, name));
                invokeRewardListeners();
            }

            @Override
            public void onRewardedVideoClosed(boolean finished) {
                invokeAdClosed();
                showToast(String.format("onRewardedVideoClosed,  finished: %s", finished));
            }

            void showToast(final String text) {

                System.out.println("Appoloving, " + text);
//                if (mToast == null) {
//                    mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
//                }
//                mToast.setText(text);
//                mToast.setDuration(Toast.LENGTH_SHORT);
//                mToast.show();
            }
        });
    }

    public void setTextToUpdate(TextView tv) {
        mTextToUpdate = tv;
    }

    public void refresh() {
        if (sRemainingTries == 0) {
            startWaitTimer();
            invokeListeners(STATE_WAITING);
        } else {
            invokeListeners(STATE_ELIGABLE_TO_PLAY);
        }
    }

    private void invokeListeners(int state) {
        Set<AdRequestStateChangeListener> keySet = sListeners.keySet();

        for (AdRequestStateChangeListener listener : keySet) {
            if (listener != null) {
                listener.onStateChange(state);
            }
        }
    }

    private void invokeRewardListeners() {
        Set<AdRequestStateChangeListener> keySet = sListeners.keySet();

        for (AdRequestStateChangeListener listener : keySet) {
            if (listener != null) {
                listener.onAdWatched();
            }
        }
    }

    private void invokeAdClosed() {
        Set<AdRequestStateChangeListener> keySet = sListeners.keySet();

        for (AdRequestStateChangeListener listener : keySet) {
            if (listener != null) {
                listener.onAdClosed();
            }
        }
    }

    public int getRemainingTries() {
        return sRemainingTries;
    }

    private void setRemainingTries(int remainingTries) {

        if (remainingTries == 0) {


            if (sRemainingTries != 0) {

                getServerTime(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        final long serverStart = (Long) dataSnapshot.getValue();

                        saveServerStart(serverStart);
                        sStartTime = System.currentTimeMillis();

                        startWaitTimer();
                        invokeListeners(STATE_WAITING);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

        } else {
            invokeListeners(STATE_ELIGABLE_TO_PLAY);
        }

        sRemainingTries = remainingTries;
//        refresh();
    }

    private static int sServerChangeTimes = 0;

    public static void getServerTime(final ValueEventListener valueEventListener) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        sServerChangeTimes = 0;
        if (user != null) {

            final DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child("server_time");

            ref.setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            valueEventListener.onDataChange(dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    valueEventListener.onCancelled(null);
                }
            });
        }
    }

    private void fetchRemainingTries(ValueEventListener listener) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child(REMAINING_TRIES_KEY);
            ref.addListenerForSingleValueEvent(listener);
        }
    }

    public static void saveServerStart(long timestamp) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child("serverStart");
            ref.setValue(timestamp);
        }
    }

    public static void fetchServerStart(ValueEventListener listener) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child("serverStart");
            ref.addListenerForSingleValueEvent(listener);
        }
    }

    public static void saveRemainingTries(int remainingTries, final OnFailureListener listener) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child(REMAINING_TRIES_KEY);
//            ref.addListenerForSingleValueEvent(listener != null ? listener : new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });
            ref.setValue(remainingTries).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (listener != null)
                        listener.onFailure(e);
                }
            });
        }
    }

    private void startWaitTimer() {
        timerHandler.postDelayed(timerRunnable, 0);

//        getSharedPreferences(DIRECTORY_NAME).edit().putLong(START_TIME_KEY, sStartTime).commit();

    }

    private boolean hasInterstitial() {
        return Appodeal.isLoaded(Appodeal.INTERSTITIAL);
    }

//    private void showInterstitial() {
//        Appodeal.show((Activity) mContext, Appodeal.INTERSTITIAL);
//    }

    public boolean requestAd() {

        if (sRemainingTries == MAX_TRIES) {
            getServerTime(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (sRemainingTries == 0)
                        return;

                    final long curServer = (long) dataSnapshot.getValue();

                    fetchServerStart(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            long serverStart = dataSnapshot.getValue() == null ? -1 : (long) dataSnapshot.getValue();

                            //getSharedPreferences(DIRECTORY_NAME).getLong("server_start", curServer - WAIT_TIME_MILIS);

                            long delta = curServer - serverStart;

                            if (delta >= WAIT_TIME_MILIS) {

                                saveServerStart(-1);
                                if (sRemainingTries > 0) {
                                    startAd();
                                } else if (sRemainingTries > 0 && hasInterstitial()) {
                                    showInterstitial();
                                }

                            } else {
                                sStartTime = System.currentTimeMillis() - delta;
                                sRemainingTries = 0;
                                saveRemainingTries(sRemainingTries, null);
                                startWaitTimer();
                                invokeListeners(STATE_WAITING);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    if (sRemainingTries > 0) {
                        startAd();
                    } else if (sRemainingTries > 0 && hasInterstitial()) {
                        showInterstitial();
                    }
                }
            });

            if (sRemainingTries > 0 ) {
                return true;
            } else if (sRemainingTries > 0 && hasInterstitial()) {
                return true;
            }
        } else {
            if (sRemainingTries > 0) {
                startAd();
                return true;
            } else if (sRemainingTries > 0 && hasInterstitial()) {
                showInterstitial();
                return true;
            }
        }

        return false;
    }

//    private OnFailureListener getTriesFailureListener() {
//        return new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//                fetchRemainingTries(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        Object value = dataSnapshot.getValue();
//
//                        if (value != null) {
//                            try {
//
//                            } catch (Exception e) {
//
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(DatabaseError databaseError) {
//
//                    }
//                });
//            }
//        };
//    }

//    private boolean hasAvailableVideoAd() {
//        return true;
////        return Appodeal.isLoaded(Appodeal.REWARDED_VIDEO);
//
////                (mUnityRewardedVideoReady && UnityAds.isReady("rewardedVideo")) || (sVunglePub.isAdPlayable())
////                || sAdColonyRewardedInterstitial != null || (mNativeXReady && MonetizationManager.isAdReady("Rewarded Video")) || Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT);
//
//    }

    public void onPause() {
        timerHandler.removeCallbacks(timerRunnable);
    }

    public void onResume() {
//        sVunglePub.onResume();
        Appodeal.onResume((Activity) mContext, Appodeal.BANNER_BOTTOM);
//        if (sRemainingTries == 0 || sRemainingTries == -1) {
//            loadData();
//        }

//        Chartboost.onResume((Activity) mContext);
    }

    public void onStop() {
//        Chartboost.onStop((Activity) mContext);
    }

    public void onStart() {

    }

    public void onDestroy() {
//        Chartboost.onDestroy((Activity) mContext);
    }

    public long getStartTime() {
        return sStartTime;
    }

    private void startAd() {
        if (AdColonyManager.IS_ADCOLONY_AVAILABLE) {
            adColonyManager.getAd().show();
        } else if (AdMobManager.IS_ADMOB_VIDEO_AVAILABLE) {
            if (adMobManager.getmRewardedVideoAd().isLoaded()) {
                adMobManager.startAdMobVideo();
            } else  {
                Toast.makeText(mContext, mContext.getString(R.string.prompt_video_is_loading), Toast.LENGTH_SHORT).show();
            }
        } else {
            if (adMobManager.getmInterstitialAd().isLoaded()) {
                adMobManager.showInterstial();
            } else {
                Toast.makeText(mContext, mContext.getString(R.string.prompt_video_is_loading), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showInterstitial() {
        // Show the ad if it's ready. Otherwise toast and restart the game.
    }

    public void setAdColonyManager(AdColonyManager adColonyManager) {
        this.adColonyManager = adColonyManager;
    }

    public void setAdMobManager(final AdMobManager adMobManager) {
        this.adMobManager = adMobManager;
        this.adMobManager.getmRewardedVideoAd().setRewardedVideoAdListener(this);
        this.adMobManager.getmInterstitialAd().setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                adMobManager.loadInterstial();
                saveRemainingTries(sRemainingTries -1, null);
                setRemainingTries(sRemainingTries - 1);
                invokeRewardListeners();
            }
        });
    }

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.d("ADMOB", "onRewardedVideoAdLoaded: ");

    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.d("ADMOB", "onRewardedVideoAdOpened: ");

    }

    @Override
    public void onRewardedVideoStarted() {
        Log.d("ADMOB", "onRewardedVideoStarted: ");

    }

    @Override
    public void onRewardedVideoAdClosed() {
        invokeAdClosed();
        Log.d("ADMOB", "onRewardedVideoAdClosed: ");
        adMobManager.loadRevardAdmobVideo();

    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                saveRemainingTries(sRemainingTries -1, null);
                setRemainingTries(sRemainingTries - 1);
                invokeRewardListeners();
            }
        });

    }

    @Override
    public void onRewardedVideoAdLeftApplication() {

    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.d("ADMOB", "onRewardedVideoAdFailedToLoad: ");
        AdMobManager.IS_ADMOB_VIDEO_AVAILABLE = false;
        adMobManager.loadInterstial();
    }

    public interface AdRequestStateChangeListener {
        void onStateChange(int state);

        void onAdWatched();

        void onAdClosed();
    }
}
