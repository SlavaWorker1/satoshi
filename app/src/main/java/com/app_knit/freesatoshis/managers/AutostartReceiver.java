package com.app_knit.freesatoshis.managers;


import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;

import static android.content.Context.ALARM_SERVICE;

public class AutostartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            Log.d("PaymentService", "We have a user");
        }

        try {
            schedulePaymentAlarm(context);
            Log.d("PaymentService", "Started After boot");
        } catch (Exception e) {
            FirebaseCrash.report(e);
            Log.d("PaymentService", "Error After boot");
        }
    }

    private void schedulePaymentAlarm(Context context) {
        Intent intent = new Intent("com.app_knit.freesatoshis.managers.PaymentReceiver");

        PendingIntent sender = PendingIntent.getBroadcast(context, 192838, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) context.getSystemService(ALARM_SERVICE);

        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 30000, AlarmManager.INTERVAL_FIFTEEN_MINUTES, sender);
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
