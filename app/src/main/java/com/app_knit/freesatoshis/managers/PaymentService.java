package com.app_knit.freesatoshis.managers;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.app_knit.freesatoshis.R;
import com.app_knit.freesatoshis.models.Payment;
import com.app_knit.freesatoshis.utils.General;
import com.coinbase.api.Coinbase;
import com.coinbase.api.CoinbaseBuilder;
import com.coinbase.api.entity.Transaction;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.money.Money;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

public class PaymentService extends IntentService {

    public static double STH_TO_BTC_CONVERSION_RATE = 0.00000001;

    private static long sCurrentlyHandledSatoshi;

    public PaymentService() {
        super("PaymentService");
    }

    private static boolean sLock = false;
    private boolean isAutomaticPayEnable = false;

    @Override
    protected void onHandleIntent(Intent intent) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

//        final FirebaseAnalytics firebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
//        {
//            Bundle bundle = new Bundle();
//            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "payment_service_started");
//            firebaseAnalytics.logEvent("payment_service", bundle);
//        }

        Log.d("PaymentService", "Started Service");

        sLock = true;

        FirebaseDatabase.getInstance().getReference("/remoteConfig/automaticPayments").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    isAutomaticPayEnable = dataSnapshot.getValue(Boolean.class);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        if (user != null ) {

            Log.d("PaymentService", "User is " + user.getUid());

            FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                        return;
                    }

                    HashMap<String, Object> dbData = ((HashMap<String, Object>) dataSnapshot.getValue());

                    if (!dbData.containsKey("satoshis")) {
                        return;
                    }

                    sCurrentlyHandledSatoshi = (long) dbData.get("satoshis");
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot == null || dataSnapshot.getValue() == null) {
                        sLock = false;
                        return;
                    }

                    long limit = RemoteConfig.getInstance(getApplicationContext()).getWithdrawLimit();
                    long upperLimit = RemoteConfig.getInstance(getApplicationContext()).getUpperLimit();
                    HashMap<String, Object> dbData = ((HashMap<String, Object>) dataSnapshot.getValue());

                    if (isAutomaticPayEnable) {
                        Log.d("PaymentService", "AutoPay: true");
                    } else {
                        Log.d("PaymentService", "AutoPay: false");
                    }

                    if (!dbData.containsKey("satoshis")) {
                        return;
                    }

                    sCurrentlyHandledSatoshi = (long) dbData.get("satoshis");

                    final String userWalletAddress = (String) dbData.get("wallet_address");

                    Log.d("PaymentService", "User with walled ADDR: " + userWalletAddress + " is requesting " + sCurrentlyHandledSatoshi);

                    if (sCurrentlyHandledSatoshi >= limit && upperLimit > sCurrentlyHandledSatoshi && !TextUtils.isEmpty(userWalletAddress)) {

                        final DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("paymentRequests").child(user.getUid());

                        db.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if (sLock) {
                                    return;
                                }

                                if (dataSnapshot.getValue() != null) {
                                    if (isAutomaticPayEnable) {
                                        makePayment(sCurrentlyHandledSatoshi, userWalletAddress);
                                    }

                                } else {

//                                    {
//                                        Bundle bundle = new Bundle();
//                                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "no_payment_req_from_user");
//                                        firebaseAnalytics.logEvent("payment_service", bundle);
//                                    }
//                                    db.child("satoshis_pending").setValue(userSatoshis, new DatabaseReference.CompletionListener() {
//                                        @Override
//                                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
//                                            FirebaseDatabase.getInstance().getReference().child("paymentRequests").addListenerForSingleValueEvent(new ValueEventListener() {
//                                                @Override
//                                                public void onDataChange(DataSnapshot dataSnapshot) {
//                                                    // TODO update total Amount here
//                                                    // FirebaseDatabase.getInstance().getReference().child("paymentRequests").child("totalAmount")
//                                                }
//
//                                                @Override
//                                                public void onCancelled(DatabaseError databaseError) {
//
//                                                }
//                                            });
//                                        }
//                                    });
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.d("PaymentService", "Db Error" + databaseError.getMessage());
                            }
                        });
                    } else {
//                        {
//                            Bundle bundle = new Bundle();
//                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "no_wallet_for_user");
//                            firebaseAnalytics.logEvent("payment_service", bundle);
//                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        } else {
//            {
//                Bundle bundle = new Bundle();
//                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "no_user_for_payment");
//                firebaseAnalytics.logEvent("payment_service", bundle);
//            }
        }
    }

    private void rollbackPayment(final String walletAddress, String id) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        final DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference()
                .child("payments")
                .child(user.getUid());

        if (id == null) {
            id = System.currentTimeMillis() + "";
        }

        final String finalId = id;
        firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Payment pendingPayment = new Payment();

                if (sCurrentlyHandledSatoshi < RemoteConfig.getInstance(getApplicationContext()).getWithdrawLimit() &&
                        sCurrentlyHandledSatoshi > RemoteConfig.getInstance(getApplicationContext()).getUpperLimit()) {
                    return;
                }

                if (dataSnapshot.getValue() == null) {

//                    pendingPayment.setPaymentDate(null);
//                    pendingPayment.setId(finalId);
//                    pendingPayment.setStatus("pending");
//                    pendingPayment.setReceiverWallet(walletAddress);
//                    pendingPayment.setSatoshisRequested(sCurrentlyHandledSatoshi);
//
//                    General.savePayment(firebaseDatabase, pendingPayment);
                } else {
                    try {
                        Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
                        boolean foundPending = false;
                        if (iterator != null) {
//                            while (iterator.hasNext()) {
//                                DataSnapshot snapshot = iterator.next();
//
//                                if (snapshot.getValue() instanceof HashMap) {
//                                    HashMap<String, Object> snapMap = (HashMap<String, Object>) snapshot.getValue();
//
//                                    String id = snapshot.getKey();
//
//                                    if (snapMap.get("status").equals("pending")) {
//                                        pendingPayment.setPaymentDate(null);
//                                        pendingPayment.setId(id);
//                                        pendingPayment.setStatus("pending");
//                                        pendingPayment.setReceiverWallet(walletAddress);
//                                        pendingPayment.setSatoshisRequested(sCurrentlyHandledSatoshi);
//
//                                        General.savePayment(firebaseDatabase, pendingPayment);
//
//                                        foundPending = true;
//                                    }
//                                }
//                            }

//                            if (!foundPending) {
//                                pendingPayment.setPaymentDate(null);
//                                pendingPayment.setId(finalId);
//                                pendingPayment.setStatus("pending");
//                                pendingPayment.setReceiverWallet(walletAddress);
//                                pendingPayment.setSatoshisRequested(sCurrentlyHandledSatoshi);
//
//                                General.savePayment(firebaseDatabase, pendingPayment);
//                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void makePayment(final long satoshis, final String walletAddress) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String id = "";
                try {

                    final Coinbase cb = new CoinbaseBuilder()
                            .withApiKey(getString(R.string.coinbase_key), getString(R.string.coinbase_secret))
                            .build();

                    final Transaction t = new Transaction();
                    t.setTo(walletAddress);
                    String money = "BTC " + new BigDecimal(satoshis * STH_TO_BTC_CONVERSION_RATE).setScale(6, BigDecimal.ROUND_HALF_UP);
                    Log.d("PaymentService", "THIS MONEY IS REQUESTED " + money);
                    t.setAmount(Money.parse(money));

                    id = t.getId();

                    cb.sendMoney(t);

                    if (id == null) {
                        id = t.getId();
                    }

                    handlePaymentSuccess(satoshis, walletAddress, id);
                } catch (Exception e) {
                    Log.d("PaymentService", e.getMessage());
                    e.printStackTrace();

                    FirebaseCrash.report(e);

                    rollbackPayment(walletAddress, id);
                }

                sLock = false;
            }
        }).start();
    }

    private void handlePaymentSuccess(final long satoshis, final String walletAddress, String id) {
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            if (id == null) {
                id = System.currentTimeMillis() + "";
            }
//
            final DatabaseReference firebaseDatabase = FirebaseDatabase.getInstance().getReference()
                    .child("payments")
                    .child(user.getUid());
//
//            final String finalId = id;
//            firebaseDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(DataSnapshot dataSnapshot) {
            Payment pendingPayment = new Payment();

//                    if (dataSnapshot.getValue() == null) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            pendingPayment.setPaymentDate(simpleDateFormat.format(new Date()));
            pendingPayment.setId(id);
            pendingPayment.setStatus("complete");
            pendingPayment.setReceiverWallet(walletAddress);
            pendingPayment.setSatoshisRequested(satoshis);

            General.savePayment(firebaseDatabase, pendingPayment);
//                    } else {
//                        try {
//                            Iterator<DataSnapshot> iterator = dataSnapshot.getChildren().iterator();
//                            if (iterator != null) {
//                                while (iterator.hasNext()) {
//                                    DataSnapshot snapshot = iterator.next();
//
//                                    if (snapshot.getValue() instanceof HashMap) {
//                                        HashMap<String, Object> snapMap = (HashMap<String, Object>) snapshot.getValue();
//
//                                        String id = snapshot.getKey();
//
//                                        if (snapMap.get("status").equals("pending")) {
//                                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
//                                            pendingPayment.setPaymentDate(simpleDateFormat.format(new Date()));
//                                            pendingPayment.setId(id);
//                                            pendingPayment.setStatus("complete");
//                                            pendingPayment.setReceiverWallet(walletAddress);
//                                            pendingPayment.setSatoshisRequested(satoshis);
//
//                                            General.savePayment(firebaseDatabase, pendingPayment);
//                                        }
//                                    }
//                                }
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }

//                @Override
//                public void onCancelled(DatabaseError databaseError) {
//
//                }
//            });


            FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    HashMap<String, Object> dbData = ((HashMap<String, Object>) dataSnapshot.getValue());
                    long curSavedSatoshis = (long) dbData.get("satoshis");

                    curSavedSatoshis -= satoshis;

                    dataSnapshot.getRef().child("satoshis").setValue(curSavedSatoshis);

                    FirebaseDatabase.getInstance().getReference().child("paymentRequests").child(user.getUid()).removeValue();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            FirebaseDatabase.getInstance().getReference().child("paymentRequests").child("totalAmount").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        FirebaseDatabase.getInstance().getReference().child("paymentRequests").child("totalAmount").setValue((long) dataSnapshot.getValue() - satoshis);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
