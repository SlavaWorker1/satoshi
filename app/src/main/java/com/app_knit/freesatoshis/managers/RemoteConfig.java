package com.app_knit.freesatoshis.managers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.app_knit.freesatoshis.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

public class RemoteConfig {
    private Context mContext;

    private static boolean sDefaultsSet = false;

    public static RemoteConfig getInstance(Context context) {
        return new RemoteConfig(context);
    }

    private RemoteConfig(Context context) {
        mContext = context;

        if (!sDefaultsSet) {
            FirebaseRemoteConfigSettings config = new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build();
            FirebaseRemoteConfig.getInstance().setConfigSettings(config);

            getRemoteConfig().setDefaults(R.xml.remotedefaults);

            sDefaultsSet = true;
        }
    }

    private FirebaseRemoteConfig getRemoteConfig() {
        return FirebaseRemoteConfig.getInstance();
    }

    public void fetchData(final OnCompleteListener<Void> listener) {
        getRemoteConfig().fetch(0).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    Log.d("RemoteConfig", "Fetch Succeeded");
                    // Once the config is successfully fetched it must be activated before newly fetched
                    // values are returned.
                    boolean result = getRemoteConfig().activateFetched();
                    Log.d("RemoteConfig", "Activated fetched: " + result);
                } else {
                    Log.d("RemoteConfig", "Fetch failed");
                }

                if (listener != null)
                    listener.onComplete(task);

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }


    public long getSatoshisPerTry() {
        return getRemoteConfig().getLong("satoshis_per_ad");
    }

    public String getLabelSatoshisPerClick() {
        return getRemoteConfig().getString("label_satoshis_per_click");
    }

    public String getLabelSatoshisPerMins() {
        return getRemoteConfig().getString("label_satoshis_per_mins");
    }

    public String getWithdrawLabel() {
        return getRemoteConfig().getString("label_withdraw_notice");
    }

    public long getUpperLimit(){ return getRemoteConfig().getLong("satoshi_sending_limit");}

    public long getWithdrawLimit(){
        return getRemoteConfig().getLong("withdraw_limit");
    }
}
