package com.app_knit.freesatoshis.managers;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app_knit.freesatoshis.MainActivity;
import com.app_knit.freesatoshis.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RemainingTriesService extends IntentService {

    public RemainingTriesService() {
        super("RemainingTriesService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        SharedPreferences sharedPreferences = getSharedPreferences(AdRequestManager.DIRECTORY_NAME, Context.MODE_PRIVATE);

        AdRequestManager.getServerTime(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final long currentTime = (long) dataSnapshot.getValue();

                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference()
                            .child("users").child(user.getUid());

                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            long startTime = dataSnapshot.child("serverStart").getValue() == null ? -1 : (long) dataSnapshot.child("serverStart").getValue();
                            Object value = dataSnapshot.child(AdRequestManager.REMAINING_TRIES_KEY).getValue();
                            int remainingTries = AdRequestManager.MAX_TRIES;

                            if (value != null) {
                                if (value instanceof Long) {
                                    remainingTries = ((Long) value).intValue();
                                } else if (value instanceof String) {
                                    remainingTries = Integer.parseInt((String) value);
                                } else {
                                    remainingTries = (int) dataSnapshot.getValue();
                                }
                            }

                            if (startTime != -1 && remainingTries == 0) {
                                long millis = currentTime - startTime;
                                long thirtyMinutes = AdRequestManager.WAIT_TIME_MILIS - millis;

                                if (thirtyMinutes <= 0) {
                                    Log.d("Service", "Running");

                                    AdRequestManager.saveRemainingTries(AdRequestManager.MAX_TRIES, null);

                                    if (Thread.currentThread().getName().equals("main")) {
                                        AdRequestManager.getInstance(getApplicationContext(), null).refresh();
                                    } else {
                                        int notifId = (int) System.currentTimeMillis();

                                        NotificationManager mNotifyMgr =
                                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                        mNotifyMgr.notify(notifId, buildNotification(notifId));
                                    }
                                }
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private Notification buildNotification(int notifId) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this.getApplicationContext())
                        .setSmallIcon(R.drawable.ic_menu_bitcoin)
                        .setContentTitle(getString(R.string.notif_title))
                        .setContentText(getString(R.string.notif_description));

        Intent resultIntent = new Intent(this, MainActivity.class);
        Bundle bundle = new Bundle();

        resultIntent.putExtras(bundle);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        notifId,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        return notification;
    }
}