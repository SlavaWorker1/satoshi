package com.app_knit.freesatoshis.managers;

import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;


public class AdMobManager  {
    private RewardedVideoAd mRewardedVideoAd;
    private InterstitialAd mInterstitialAd;
    static boolean IS_ADMOB_VIDEO_AVAILABLE = true;
    public static final String AD_APP_ID = "ca-app-pub-7818888802530065~6652909636";
    private static final String AD_VIDEO = "ca-app-pub-7818888802530065/7630505215";
    private static final String INTREST_ID = "ca-app-pub-7818888802530065/3845149355";
    public static AdRequest adRequest;

    public AdMobManager(Context context) {
        this.mRewardedVideoAd =  MobileAds.getRewardedVideoAdInstance(context);
        adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .addTestDevice("41F5AFDD4DD393A71313C4CFDE862E65")
                .addTestDevice("928E8CCC1C0769ED3923CB6E8AB5CA84").build();

        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(INTREST_ID);
        loadRevardAdmobVideo();
        loadInterstial();
    }

    void loadRevardAdmobVideo() {
        if (!mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.loadAd(AD_VIDEO, adRequest);
        }
    }

    void loadInterstial(){
        if (!mInterstitialAd.isLoading() && !mInterstitialAd.isLoaded()) {
            mInterstitialAd.loadAd(adRequest);
        }
    }

    RewardedVideoAd getmRewardedVideoAd() {
        return mRewardedVideoAd;
    }

    void startAdMobVideo() {
        if (mRewardedVideoAd.isLoaded()) {
            mRewardedVideoAd.show();
        }
    }

    void showInterstial() {
        mInterstitialAd.show();
    }

    public InterstitialAd getmInterstitialAd() {
        return mInterstitialAd;
    }
}
