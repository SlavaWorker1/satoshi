package com.app_knit.freesatoshis.managers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Calendar;
/**
 * Receiver for the Notifications background service
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static final int REQUEST_CODE = 12345;

    public AlarmReceiver() {
        super();
    }

    // Triggered by the Alarm periodically (starts the service to run task)
    @Override
    public void onReceive(Context context, Intent intent) {

        Intent i = new Intent(context, RemainingTriesService.class);
        i.putExtra("time", Calendar.getInstance().getTimeInMillis());
        context.startService(i);
    }
}