package com.app_knit.freesatoshis.managers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.app_knit.freesatoshis.FirebaseMessageAcitivity;
import com.app_knit.freesatoshis.MainActivity;
import com.app_knit.freesatoshis.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FirebaseNotificationReceiver extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("AdMan", "I AM CREATED - GOOD");
    }

    @Override
    public boolean onUnbind(Intent intent) {

        Log.d("AdMan", "I UNBOUND - BAD");
        return super.onUnbind(intent);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d("AdMan", "I AM DESTROYED - BAD");
    }

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);

        Log.d("AdMan", "ERROR " + s);
        e.printStackTrace();
    }



    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

        Log.d("AdMan", "Started Intent: " + intent.getData());
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
        }

        if (remoteMessage.getData().containsKey("new_version")) {
            Intent intent = new Intent(this, FirebaseMessageAcitivity.class );
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            Intent notificationIntent = new Intent(getApplicationContext(), MainActivity.class);
            notificationIntent.putExtra("title", remoteMessage.getData().get("title"));
            notificationIntent.putExtra("body", remoteMessage.getData().get("body"));
            notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


            if (remoteMessage.getData().containsKey("open_url")) {
                String urlToOpen = remoteMessage.getData().get("open_url");
                notificationIntent.putExtra("open_url", urlToOpen);
            }
            sendNotification(remoteMessage, (int) remoteMessage.getSentTime());

            startActivity(notificationIntent);

            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param notification
     */
    private void sendNotification(RemoteMessage notification, int id) {
        Intent intent = null;

        if (notification.getData().containsKey("open_url")) {
            String urlToOpen = notification.getData().get("open_url");
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(urlToOpen));
        } else {

            intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_menu_bitcoin)
                .setContentTitle(notification.getData().get("title"))
                .setContentText(notification.getData().get("body"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(id, notificationBuilder.build());
    }
}
