package com.app_knit.freesatoshis.managers;

import android.util.Log;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyZone;



public class AdColonyManager {
    public static boolean IS_ADCOLONY_AVAILABLE = true;
    public final static String ADCOLONY_APP_ID = "app2bab335a887c41ba87";
    public final static String AD_COLONY_ZONE_ID = "vz5c39508e131b45ce91";
    private AdColonyAdOptions ad_options;
    private AdColonyInterstitial ad;
    private AdColonyInterstitialListener listener;
    private final String TAG = AdColonyManager.class.getSimpleName();

    public AdColonyInterstitial getAd() {
        return ad;
    }

    public AdColonyInterstitialListener getListener() {
        return listener;
    }

    public AdColonyAdOptions getAd_options() {
        return ad_options;
    }

    public AdColonyManager() {
        /* Ad specific options to be sent with request */
        ad_options = new AdColonyAdOptions();

        /*
          Set up listener for interstitial ad callbacks. You only need to implement the callbacks
          that you care about. The only required callback is onRequestFilled, as this is the only
          way to get an ad object.
         */
        listener = new AdColonyInterstitialListener()
        {
            /** Ad passed back in request filled callback, ad can now be shown */
            @Override
            public void onRequestFilled( AdColonyInterstitial ad )
            {
                AdColonyManager.this.ad = ad;
                Log.d( TAG, "onRequestFilled" );
            }

            /** Ad request was not filled */
            @Override
            public void onRequestNotFilled( AdColonyZone zone )
            {
                Log.d( TAG, "onRequestNotFilled");
                IS_ADCOLONY_AVAILABLE = false;
            }

            /** Ad opened, reset UI to reflect state change */
            @Override
            public void onOpened( AdColonyInterstitial ad )
            {
                Log.d( TAG, "onOpened" );
            }

            /** Request a new ad if ad is expiring */
            @Override
            public void onExpiring( AdColonyInterstitial ad )
            {
                AdColony.requestInterstitial(AD_COLONY_ZONE_ID, this, ad_options );
                Log.d( TAG, "onExpiring" );
            }
        };




    }
}
