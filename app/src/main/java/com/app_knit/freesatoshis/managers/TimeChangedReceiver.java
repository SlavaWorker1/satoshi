package com.app_knit.freesatoshis.managers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class TimeChangedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        long startTime = System.currentTimeMillis();
        AdRequestManager.setStartTime(startTime, context);
    }
}