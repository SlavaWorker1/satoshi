package com.app_knit.freesatoshis.managers;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.app_knit.freesatoshis.MainActivity;
import com.app_knit.freesatoshis.R;

public class PaymentReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, PaymentService.class));


//        NotificationManager mNotifyMgr =
//                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//        mNotifyMgr.notify(12, buildNotification(12, context));
    }

    private Notification buildNotification(int notifId, Context context) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context.getApplicationContext())
                        .setSmallIcon(R.drawable.ic_menu_bitcoin)
                        .setContentTitle(context.getString(R.string.notif_title))
                        .setContentText("Payment Started");

        Intent resultIntent = new Intent(context, MainActivity.class);
        Bundle bundle = new Bundle();

        resultIntent.putExtras(bundle);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        context,
                        notifId,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        return notification;
    }
}
