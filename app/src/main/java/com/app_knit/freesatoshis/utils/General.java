package com.app_knit.freesatoshis.utils;


import com.app_knit.freesatoshis.models.Payment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

public class General {

    public static void savePayment(DatabaseReference reference, Payment payment) {

        reference = reference.child(payment.getId());

        reference.child("walletAddress").setValue(payment.getReceiverWallet());
        reference.child("status").setValue(payment.getStatus());
        reference.child("satoshis").setValue(payment.getSatoshisRequested());
        reference.child("date").setValue(payment.getPaymentDate());
    }

    public static Payment paymentFromSnapshot(DataSnapshot snapshot) {
        Payment payment = new Payment();

        try {
            payment.setId(snapshot.getKey());
            payment.setStatus((String) snapshot.child("status").getValue());
            payment.setSatoshisRequested((Long) snapshot.child("satoshis").getValue());
            payment.setReceiverWallet((String) snapshot.child("walletAddress").getValue());
            payment.setPaymentDate((String) snapshot.child("date").getValue());
        } catch (Exception e){
            e.printStackTrace();
        }

        return payment;
    }
}
