package com.app_knit.freesatoshis;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAppOptions;
import com.app_knit.freesatoshis.managers.AdColonyManager;
import com.app_knit.freesatoshis.managers.AdMobManager;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /* Construct optional app options object to be sent with configure */
        AdColonyAppOptions app_options = new AdColonyAppOptions()
                .setUserID( "unique_user_id" );
        /*
          Configure AdColony in your launching Activity's onCreate() method so that cached ads can
          be available as soon as possible.
         */
        AdColony.configure( this, app_options, AdColonyManager.ADCOLONY_APP_ID, AdColonyManager.AD_COLONY_ZONE_ID );

        // Initialize admob
        MobileAds.initialize(this, AdMobManager.AD_APP_ID);
 
        FirebaseMessaging.getInstance().subscribeToTopic("app_version");

        FirebaseDatabase.getInstance().getReference().child("appVersion").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String actualVersion = (String) dataSnapshot.getValue();
                String localVersion  = BuildConfig.VERSION_NAME;
                if (!(actualVersion).equals(localVersion)) {
                    Intent intent = new Intent(SplashScreen.this, FirebaseMessageAcitivity.class );
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }
}
