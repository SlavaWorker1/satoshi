package com.app_knit.freesatoshis.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.app_knit.freesatoshis.R;

import java.util.Random;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CaptchaFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CaptchaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CaptchaFragment extends DialogFragment {

    private OnFragmentInteractionListener mListener;
    private String mCaptchaSymbols;
    private EditText mCaptchaEnter;

    public CaptchaFragment() {
        // Required empty public constructor
    }

    public static CaptchaFragment newInstance() {
        CaptchaFragment fragment = new CaptchaFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    public void setListener(OnFragmentInteractionListener listener){
        mListener = listener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View mainView = inflater.inflate(R.layout.fragment_captcha, container, false);

        String captchaSymbols = "";
        Random r = new Random();

        for (int i = 0; i < 6; i++) {
            int code = r.nextInt(91 - 65) + 65;

            captchaSymbols += (char) code;
        }

        TextView captcha = (TextView) mainView.findViewById(R.id.tv_captcha_symbols);
        captcha.setText(captchaSymbols);

        mCaptchaSymbols = captchaSymbols;
        mCaptchaEnter = (EditText) mainView.findViewById(R.id.et_captcha);

        Button sumbitBtn = (Button) mainView.findViewById(R.id.btn_submit);
        sumbitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                onSubmitCaptcha(mCaptchaEnter.getText().toString());
            }
        });

        return mainView;
    }

    private void onSubmitCaptcha(String submitted) {

        mCaptchaEnter.setError(null);

        if(mCaptchaSymbols.toLowerCase().equals(submitted.toLowerCase())){
            mListener.onCaptchaSuccess();
            dismissAllowingStateLoss();
        } else {
            mCaptchaEnter.setError("Wrong symbols");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
       void onCaptchaSuccess();
    }
}
