package com.app_knit.freesatoshis.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app_knit.freesatoshis.R;
import com.app_knit.freesatoshis.models.Payment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.PaymentHistoryViewHolder> {

    private ArrayList<Payment> mPaymentHistory;

    public PaymentHistoryAdapter() {
        mPaymentHistory = new ArrayList<>();
    }

    @Override
    public PaymentHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_history_item, parent, false);
        return new PaymentHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentHistoryViewHolder parent, int position) {
        Payment item = mPaymentHistory.get(position);

        if (item != null) {
            parent.mStatusView.setText(item.getStatus().toUpperCase());
            parent.mSatoshisView.setText(item.getSatoshisRequested() + "");
            parent.mDateView.setText(item.getPaymentDate() == null ? " - " : item.getPaymentDate());
            parent.mWalletView.setText(item.getReceiverWallet());
        }
    }

    public void setItems(ArrayList<Payment> items) {


        Collections.sort(items, new Comparator<Payment>() {
            @Override
            public int compare(Payment payment, Payment t1) {
                if(payment.getStatus().equals("pending")){
                    return -1;
                }

                if(t1.getStatus().equals("pending")){
                    return 1;
                }

                if(payment.getPaymentDate() != null && t1.getPaymentDate() != null){
                    String[] splitLhs = payment.getPaymentDate().split("-");
                    String[] splitRhs = t1.getPaymentDate().split("-");

                    Calendar lhs = Calendar.getInstance();
                    lhs.set(Integer.parseInt(splitLhs[0]), Integer.parseInt(splitLhs[1]), Integer.parseInt(splitLhs[2]));

                    Calendar rhs = Calendar.getInstance();
                    rhs.set(Integer.parseInt(splitRhs[0]), Integer.parseInt(splitRhs[1]), Integer.parseInt(splitRhs[2]));

                    return rhs.compareTo(lhs);
                }

                return 0;
            }
        });

        mPaymentHistory = items;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mPaymentHistory.size();
    }

    public class PaymentHistoryViewHolder extends RecyclerView.ViewHolder {

        public final View mMainView;
        public final TextView mStatusView;
        public final TextView mSatoshisView;
        public final TextView mDateView;
        public final TextView mWalletView;

        public PaymentHistoryViewHolder(View itemView) {
            super(itemView);

            mMainView = itemView;
            mStatusView = (TextView) mMainView.findViewById(R.id.tv_status);
            mSatoshisView = (TextView) mMainView.findViewById(R.id.tv_satoshis);
            mDateView = (TextView) mMainView.findViewById(R.id.tv_date);
            mWalletView = (TextView) mMainView.findViewById(R.id.tv_wallet_address);
        }
    }
}
