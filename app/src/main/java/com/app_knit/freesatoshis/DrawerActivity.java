package com.app_knit.freesatoshis;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class DrawerActivity extends AuthActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static Bitmap sUserImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onAuthenticatedAction() {
        View headerLayout = ((NavigationView) findViewById(R.id.nav_view)).getHeaderView(0);

        TextView userEmail = (TextView) headerLayout.findViewById(R.id.textView);
        if (getUser() != null && getUser().getEmail() != null) {
            userEmail.setText(getUser().getEmail());

        }

        if (getUser() != null && getUser().getPhotoUrl() != null) {
            Uri url = getUser().getPhotoUrl();
            if (url != null) {
                setBitmapFromURL(getUser().getPhotoUrl().toString(), (ImageView) findViewById(R.id.imageView));
            }
        }
    }

    public void setBitmapFromURL(final String src, final ImageView imageView) {

        if (sUserImage != null) {
            if (imageView != null) {
                imageView.setImageBitmap(sUserImage);
            }
            return;
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(src);
                    HttpURLConnection connection = (HttpURLConnection) url
                            .openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    final Bitmap myBitmap = BitmapFactory.decodeStream(input);

                    sUserImage = myBitmap;

                    DrawerActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (imageView != null) {
                                imageView.setImageBitmap(myBitmap);
                            }
                        }
                    });

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_home && !(this instanceof MainActivity)) {
            startActivity(new Intent(this, MainActivity.class));
        } else if (id == R.id.nav_bitcoin) {
            startActivity(new Intent(this, WithdrawActivity.class));
        } else if (id == R.id.nav_history) {
            startActivity(new Intent(this, HistoryActivity.class));
        } else if (id == R.id.nav_info) {
            startActivity(new Intent(this, InfoActivity.class));
        } else if (id == R.id.nav_contact) {
            startActivity(new Intent(this, ContactActivity.class));
        } else if (id == R.id.nav_logout) {
            Intent logout = new Intent(this, LoginActivity.class);
            logout.putExtra("is_logout", true);

            startActivity(logout);

        }

        if (!(this instanceof MainActivity) && id != R.id.nav_logout) {
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
