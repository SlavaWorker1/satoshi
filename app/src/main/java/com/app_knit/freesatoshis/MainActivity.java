package com.app_knit.freesatoshis;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.adcolony.sdk.AdColony;
import com.app_knit.freesatoshis.fragments.CaptchaFragment;
import com.app_knit.freesatoshis.managers.AdColonyManager;
import com.app_knit.freesatoshis.managers.AdMobManager;
import com.app_knit.freesatoshis.managers.AdRequestManager;
import com.app_knit.freesatoshis.managers.RemoteConfig;
import com.chartboost.sdk.Chartboost;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Iterator;

public class MainActivity extends DrawerActivity implements CaptchaFragment.OnFragmentInteractionListener {


    private AdColonyManager adColonyManager;
    private AdMobManager adMobManager;


    private final String TAG = "DEBUG_ADD";

    private TextView mAdsRemainingView;
    private TextView mTimeRemainingView;
    private AdRequestManager mAdsRequestManager;
    private ImageButton mStartAdButton;
    private AdView mAdView;
    private ProgressBar mProgressBar;

    private long mSatoshis = 0;

    private AlertDialog mCurrentDialog;
    private DatabaseReference mDatabase;

    private TextView mAccumulatedSatoshis;

    private boolean mPendingReward;
    private AdRequestManager.AdRequestStateChangeListener mAdListener;
    private boolean mResumed;

    private void showRewardDialog() {

        System.out.println("Appodeal, showRewardDialog " + Thread.currentThread().getName());
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Satoshis claimed!");
        alertDialog.setMessage("You have collected " + AdRequestManager.SATOSHIS_PER_TRY + " satoshis");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
//                mAdsRequestManager.showInterstitial();
                mCurrentDialog = null;
            }
        });

//        Log.d("AdMan", "AD wants to be shown");
        mCurrentDialog = alertDialog;

        if (!MainActivity.this.isFinishing()) {
            System.out.println("Appodeal, SHOWING DIALOG " + Thread.currentThread().getName());
            alertDialog.show();
        } else {
            System.out.println("Appodeal, NOT SHOWING DIALOG " + Thread.currentThread().getName());
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (mAdsRequestManager != null) {
            mAdsRequestManager.onStart();
            mAdsRequestManager.refresh();
        }
    }

    private AdRequestManager.AdRequestStateChangeListener getListener() {
        return new AdRequestManager.AdRequestStateChangeListener() {
            @Override
            public void onStateChange(int state) {
                if (state == AdRequestManager.STATE_WAITING) {
                    // Start Timer
                    mAdsRemainingView.setVisibility(View.GONE);
                    mTimeRemainingView.setVisibility(View.VISIBLE);

                } else {
                    updateUI();
                    mTimeRemainingView.setVisibility(View.GONE);
                    mAdsRemainingView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAdWatched() {

//                System.out.println("Appodeal, OnAdWatched " + mResumed);

                mSatoshis += AdRequestManager.SATOSHIS_PER_TRY;
//                mPendingReward = true;
                saveSatoshis();

//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    System.out.println("Appodeal, OnAdWatched ++RUN" + Thread.currentThread().getName());
//
//                    saveSatoshis();
//
//                    showRewardDialog();
//
//
//                }
//            });
            }

            @Override
            public void onAdClosed() {
                try {

                    if (mCurrentDialog != null && !mCurrentDialog.isShowing()) {
                        System.out.println("Appoloving, " + " ON AD CLOSED OPEN");
                        mCurrentDialog.show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        adColonyManager = new AdColonyManager();
        adMobManager = new AdMobManager(this);

        mAdView = (AdView) findViewById(R.id.appodealBannerView);
        mAdView.loadAd(AdMobManager.adRequest);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle(getString(R.string.app_name));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mAdsRemainingView = (TextView) findViewById(R.id.tv_tries_remaining);
        mTimeRemainingView = (TextView) findViewById(R.id.tv_time_remaining);
        mAccumulatedSatoshis = (TextView) findViewById(R.id.tv_satoshis_accumulated);

        mStartAdButton = (ImageButton) findViewById(R.id.btn_start_ad);
        mStartAdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "RemainingTries: " + mAdsRequestManager.getRemainingTries());
                if (mAdsRequestManager.getRemainingTries() == AdRequestManager.MAX_TRIES) {
                    showCaptcha();
                } else {
                    requestAd();
                }
            }
        });


        mProgressBar = (ProgressBar) findViewById(R.id.pb_main);
        showHideLoading(true);

        FirebaseDatabase.getInstance().getReference().child("remoteConfig").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                showHideLoading(false);
                AdRequestManager.SATOSHIS_PER_TRY = Long.parseLong(dataSnapshot.child("satoshis_per_ad").getValue() + "");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showHideLoading(false);
            }
        });

        Button shareButton = (Button) findViewById(R.id.btn_share);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareApp();
            }
        });

        Button rateUsButton = (Button) findViewById(R.id.btn_rate);
        rateUsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rateApp();
            }
        });

        scheduleAlarm();

        checkIsFromNotif(getIntent());

        String appKey = "d83c2ace3fba348955f0292151a161641422dcd740f4d0cb";

        mAdListener = getListener();
        mAdsRequestManager = AdRequestManager.getInstance(this, mAdListener);
        mAdsRequestManager.setTextToUpdate(mTimeRemainingView);
        mAdsRequestManager.setAdColonyManager(adColonyManager);
        mAdsRequestManager.setAdMobManager(adMobManager);
    }

    private void showCaptcha() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        CaptchaFragment captchaFragment = CaptchaFragment.newInstance();

        captchaFragment.setListener(this);
        captchaFragment.show(fragmentManager, "CAPTCHA_FRAGMENT");
    }

    private void rateApp() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }

    private void shareApp() {

        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, "Share Free Bitcoin Giver");
        String sAux = "\nGet free BitCoins! \n\n";
        sAux = sAux + "https://play.google.com/store/apps/details?id=com.app_knit.freesatoshis \n\n";
        i.putExtra(Intent.EXTRA_TEXT, sAux);
        startActivity(Intent.createChooser(i, "Share with"));


//        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//        sharingIntent.setType("text/plain");
//        String shareBody = "Get free BitCoins now !";
//        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Share Free Satoshis");
//        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
//        startActivity(Intent.createChooser(sharingIntent, "Share with"));
    }

    private void checkIsFromNotif() {
        checkIsFromNotif(getIntent());
    }

    private void checkIsFromNotif(Intent intent) {

        try {

            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.containsKey("open_url")) {
                String urlToOpen = bundle.getString("open_url");
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlToOpen));
                startActivity(intent);
            }

            String title = "";
            if (intent.hasExtra("title")) {
                title = intent.getStringExtra("title");
            } else if (bundle != null && bundle.containsKey("title")) {
                title = bundle.getString("title");
            }

            String body = "";

            if (intent.hasExtra("body")) {
                body = intent.getStringExtra("body");
                Log.d("MainActivity", body);
            } else if (bundle != null && bundle.containsKey("body")) {
                body = bundle.getString("body");
            }

            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(body)) {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle(title);
                alertDialog.setMessage(body);
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                    }
                });
                alertDialog.show();
            }
        } catch (Exception e) {

        }
    }

    private void updatePaymentInfo(final String walletAddress) {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user == null || mSatoshis == 0)
            return;

        if (mSatoshis < RemoteConfig.getInstance(MainActivity.this).getWithdrawLimit()) {
            return;
        }
        FirebaseDatabase.getInstance().getReference().child("paymentRequests").child(user.getUid()).child("satoshis_pending").setValue(mSatoshis);
    }

    private void updateValue() {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid()).child("wallet_address").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String walletAddress = (String) dataSnapshot.getValue();
                if (walletAddress != null) {
                    updatePaymentInfo(walletAddress);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        if (mSatoshis >= RemoteConfig.getInstance(MainActivity.this).getWithdrawLimit()) {

            FirebaseDatabase.getInstance().getReference().child("paymentRequests").child(user.getUid()).child("satoshis_pending").setValue(mSatoshis, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    FirebaseDatabase.getInstance().getReference().child("paymentRequests").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            long totalAmount = 0;

                            if (dataSnapshot.getChildren() != null) {
                                Iterator<DataSnapshot> userValues = (Iterator<DataSnapshot>) dataSnapshot.getChildren().iterator();
                                if (userValues != null) {
                                    while (userValues.hasNext()) {
                                        DataSnapshot userSnap = userValues.next();
                                        if (userSnap.getValue() != null) {
                                            if (userSnap.getKey().equals("totalAmount"))
                                                continue;

                                            totalAmount += ((HashMap<String, Long>) userSnap.getValue()).get("satoshis_pending");
                                        }
                                    }
                                }

                                FirebaseDatabase.getInstance().getReference().child("paymentRequests").child("totalAmount").setValue(totalAmount);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            });
        }
    }

    private void saveSatoshis() {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

            showHideLoading(true);

            if (mDatabase != null) {
                mDatabase.setValue(mSatoshis, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                        showHideLoading(false);

                        if (databaseError == null) {
                            updateUI();

                            updateValue();

                            showRewardDialog();
                        } else {
                            Toast.makeText(MainActivity.this, "Couldn't save satoshis", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        checkIsFromNotif(intent);

//        AlertDialog alertDialog = new AlertDialog.Builder(getApplicationContext()).create();
//        alertDialog.setTitle();
//        alertDialog.setMessage(remoteMessage.getNotification().getBody());
//        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//
//        alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//            }
//        });
//        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();

        System.out.println("Appodeal, ON PAUSE");

        if (mAdsRequestManager != null) {
            mAdsRequestManager.onPause();
        }

        if (mCurrentDialog != null) {
            mCurrentDialog.dismiss();
        }

        mResumed = false;

//        Log.d("AdMan", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (mAdsRequestManager != null) {
            mAdsRequestManager.onStop();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mAdsRequestManager != null) {
            mAdsRequestManager.onDestroy();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mResumed = true;
        System.out.println("Appodeal ONRESUME " + mPendingReward);

        if (mAdsRequestManager != null) {
            mAdsRequestManager.onResume();
            mAdsRequestManager.refresh();
            updateUI();
        }

        if (adColonyManager.getAd() == null || adColonyManager.getAd().isExpired())
        {
            /**
             * Optionally update location info in the ad options for each request:
             * LocationManager location_manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
             * Location location = location_manager.getLastKnownLocation( LocationManager.GPS_PROVIDER );
             * ad_options.setUserMetadata( ad_options.getUserMetadata().setUserLocation( location ) );
             */
            AdColony.requestInterstitial( AdColonyManager.AD_COLONY_ZONE_ID, adColonyManager.getListener(), adColonyManager.getAd_options() );
        }
    }


    private void updateUI() {
        String remainingText = "";

        if (mAdsRequestManager.getRemainingTries() == -1) {
            mAdsRemainingView.setVisibility(View.INVISIBLE);
        } else {
            remainingText = mAdsRequestManager.getRemainingTries() + "";

            if (mAdsRequestManager.getRemainingTries() > 0)
                mAdsRemainingView.setVisibility(View.VISIBLE);
        }

        remainingText += "/" + AdRequestManager.MAX_TRIES;

        mAdsRemainingView.setText(remainingText);
        mAccumulatedSatoshis.setText(getString(R.string.label_satoshis_number) + " " + mSatoshis);
    }

    private void requestAd() {
        if (!mAdsRequestManager.requestAd()) {
            if (mAdsRequestManager.getRemainingTries() == 0) {
                Toast.makeText(this, getString(R.string.prompt_check_back_later), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, getString(R.string.prompt_video_is_loading), Toast.LENGTH_SHORT).show();
            }
        }

        updateUI();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (Chartboost.onBackPressed())
            return;
        else
            super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void schedulePaymentAlarm() {
        Intent intent = new Intent("com.freesatoshis.paymentaction");

        PendingIntent sender = PendingIntent.getBroadcast(this, 192838, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AlarmManager.INTERVAL_FIFTEEN_MINUTES, sender);
    }

    private void scheduleAlarm() {

        Intent intent = new Intent("com.app_knit.freesatoshis.managers.AlarmReceiver");

        PendingIntent sender = PendingIntent.getBroadcast(this, 192837, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);

        am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 60000, sender);

        schedulePaymentAlarm();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    private void showHideLoading(boolean show) {
        mProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
//        findViewById(R.id.rl_content_holder).setVisibility(show ? View.GONE : View.VISIBLE);
    }

    @Override
    protected void onAuthenticatedAction() {
        super.onAuthenticatedAction();

//        if (mAdsRequestManager != null) {
//            return;
//        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            mDatabase = FirebaseDatabase.getInstance().getReference()
                    .child("users").child(user.getUid()).child("satoshis");

            if (user.getEmail() != null) {
                FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid()).child("email").setValue(user.getEmail());
            }
        }
        if (mDatabase != null) {

            mDatabase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mSatoshis = dataSnapshot.getValue() != null ? dataSnapshot.getValue(Long.class) : 0;
                    updateUI();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

        final TextView labelSatoshisPerClick = (TextView) findViewById(R.id.tv_satoshis_per_click);
        final TextView labelSatoshisPerMinutes = (TextView) findViewById(R.id.tv_satoshis_per_minutes);

        FirebaseDatabase.getInstance().getReference().child("remoteConfig").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                labelSatoshisPerClick.setText(dataSnapshot.child("label_satoshis_per_click").getValue().toString());
                labelSatoshisPerMinutes.setText(dataSnapshot.child("label_satoshis_per_mins").getValue().toString());
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onCaptchaSuccess() {
        requestAd();
    }
}
