package com.app_knit.freesatoshis;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app_knit.freesatoshis.managers.RemoteConfig;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class WithdrawActivity extends DrawerActivity {

    private Button mWithdrawButton;
    private Button mRegisterButton;
    private EditText mWalletAddressView;
    private long mSatoshis = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_withdraw);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        toolbar.setTitle("Withdraw");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mWithdrawButton = (Button) findViewById(R.id.btn_withdraw);
        mWalletAddressView = (EditText) findViewById(R.id.et_walled_address);

        mRegisterButton = (Button) findViewById(R.id.btn_register);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String urlToOpen = "https://www.coinbase.com/join/58270f0f01bc8b5ed4762ea3";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(urlToOpen));
                startActivity(intent);

            }
        });

        mWithdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setWithdrawAddres();
            }
        });
    }

    public static boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private void setWithdrawAddres() {
        final String walletAddress = mWalletAddressView.getText().toString();

        mWalletAddressView.setError(null);

        if (TextUtils.isEmpty(walletAddress)) {
            mWalletAddressView.setError("Can't be empty");
        } else if (!isEmailValid(walletAddress)) {
            mWalletAddressView.setError("Invalid email");
            Toast.makeText(this, "Please Enter and Save your COINBASE LOGIN EMAIL only", Toast.LENGTH_SHORT).show();
        } else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user == null) {
                Toast.makeText(this, "An error occured please restart the application", Toast.LENGTH_SHORT).show();
                return;
            }

            FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid()).child("wallet_address").setValue(walletAddress, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                    Toast.makeText(WithdrawActivity.this, walletAddress + " set as wallet address", Toast.LENGTH_SHORT).show();
                    mWalletAddressView.setText(walletAddress);
                }
            });
        }
    }

    @Override
    protected void onAuthenticatedAction() {
        super.onAuthenticatedAction();

        final TextView withdrawLabel = (TextView) findViewById(R.id.tv_withdraw_notice);

        FirebaseDatabase.getInstance().getReference().child("remoteConfig").child("label_withdraw_notice").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    withdrawLabel.setText(dataSnapshot.getValue().toString());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        withdrawLabel.setText(RemoteConfig.getInstance(this).getWithdrawLabel());

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid()).child("wallet_address").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String walletAddress = (String) dataSnapshot.getValue();
                    if (walletAddress != null) {
                        mWalletAddressView.setText(walletAddress);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
}
