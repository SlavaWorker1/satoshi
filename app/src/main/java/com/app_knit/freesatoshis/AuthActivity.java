package com.app_knit.freesatoshis;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.app_knit.freesatoshis.managers.InstanceIdService;
import com.app_knit.freesatoshis.managers.RemoteConfig;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.iid.FirebaseInstanceId;

public abstract class AuthActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        final Thread.UncaughtExceptionHandler oldHandler =
//                Thread.getDefaultUncaughtExceptionHandler();
//
//        Thread.setDefaultUncaughtExceptionHandler(
//                new Thread.UncaughtExceptionHandler() {
//                    @Override
//                    public void uncaughtException(
//                            Thread paramThread,
//                            Throwable paramThrowable
//                    ) {
//                        FirebaseCrash.report(paramThrowable);
//
//                        if (oldHandler != null)
//                            oldHandler.uncaughtException(
//                                    paramThread,
//                                    paramThrowable
//                            ); //Delegates to Android's error handling
//                        else
//                            System.exit(2); //Prevents the service/app from freezing
//                    }
//                });

    }

    @Override
    protected void onResume() {
        super.onResume();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, LoginActivity.class));
            finish();
        } else {

            String token = FirebaseInstanceId.getInstance().getToken();
            if (token != null) {
                InstanceIdService.sendRegistrationToServer(token);
            }

            RemoteConfig.getInstance(this).fetchData(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    onAuthenticatedAction();
                }
            });
        }
    }

    protected FirebaseUser getUser() {
        return FirebaseAuth.getInstance().getCurrentUser();
    }

    protected abstract void onAuthenticatedAction();
}
